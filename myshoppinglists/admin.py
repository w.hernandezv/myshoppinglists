from django.contrib import admin

from .models import *

admin.site.register(shoppingList)
admin.site.register(product)
admin.site.register(shop)