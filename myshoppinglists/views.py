from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from .models import *
from . import nan
from django.core import serializers
import json

#Renders
def _index(request):
    shoppinglists = None
    if request.user.is_authenticated:
        shoppinglists = shoppingList.objects.filter(user = request.user)
        nose = nan.nan()
        nose.printname()
        return render(request, 'index.html', {'shoppinglists':shoppinglists})
    else:
        return render(request, 'index.html', {})

def _signin(request):
    if request.user.is_authenticated:
        return redirect('/')
    else:
        return render(request, 'signin.html')

def _signup(request):
    if request.user.is_authenticated:
        return redirect('/')
    else:
        return render(request, 'signup.html')

def _shoppingListRead(request, id):
    slist = shoppingList.objects.get(id = id)
    if request.user == slist.user:
        shops = shop.objects.all()
        prod = product.objects.filter(shopping_list = slist)
        real = 0
        budgeted = 0
        for x in prod:
            real = real + x.realcost
            budgeted = budgeted + x.budgetedcost

        return render(request, 'shoppinglist.html', {'slist':slist,'prod':prod,'idlist':id,'budgeted':budgeted,'real':real,'shops':shops})
    else:
        return redirect('/')

def _productEdit(request, idlist, id):
    slist = shoppingList.objects.get(id = idlist)
    if request.user == slist.user:
        shops = shop.objects.all()
        slist = shoppingList.objects.get(id = idlist)
        prod = product.objects.get(id = id)
        return render(request, 'product.html', {'slist':slist,'prod':prod,'shops':shops})
    else:
        return redirect('/')

#Not Renders
def _create(request):
    username = request.POST.get('username')
    email = request.POST.get('email')
    password = request.POST.get('password')
    user = User.objects.create_user(username = username, email = email, password = password)
    user.save()
    return HttpResponse('Success')

def _login(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(request, username = username, password = password)
    if user is not None:
        login(request, user)
        return redirect('/')
    else:
        return HttpResponse('<h1>User or password not valid, click <a href="/signin/">here</a> to try again.</h1>')

def _logout(request):
    logout(request)
    return redirect('/')

def _shoppingListCreate(request):
    if request.user.is_authenticated:
        name = request.POST.get('name')
        newShoppingList = shoppingList(user = request.user, name = name, completed = False)
        newShoppingList.save()
        return redirect('/')
    else:
        return redirect('/')
        
def _shoppingListUpdate(request):
    if request.user.is_authenticated:
        name = request.POST.get('name')
        newShoppingList = shoppingList(user = request.user, name = name, completed = False)
        newShoppingList.save()
        return redirect('/')
    else:
        return redirect('/')

def _shoppingListDelete(request, id):
    if request.user.is_authenticated:
        slist = shoppingList.objects.get(id = id)
        if request.user == slist.user:
            slist.delete()
            return redirect('/')
        else:
            return redirect('/')
    else:
        return redirect('/')

def _productAdd(request, idlist):
    if request.user.is_authenticated:
        shopping_list = shoppingList.objects.get(id = idlist)
        name = request.POST.get('name')
        budgetedcost = request.POST.get('budgetedcost')
        realcost = request.POST.get('realcost')

        shopid = request.POST.get('shop')
        _shop = shop.objects.get(id = shopid)
        notes = request.POST.get('notes')
        
        prod = product(shopping_list = shopping_list, name = name, budgetedcost = budgetedcost, realcost = realcost, shop = _shop, notes = notes)
        prod.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    else:
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def _productDelete(request, idlist, id):
    if request.user.is_authenticated:
        slist = shoppingList.objects.get(id = idlist)
        if slist.user == request.user:
            prod = product.objects.get(id = id)
            prod.delete()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:
            return redirect('/')
    else:
        return redirect('/')
    
def _productEditSave(request, idlist, id):
    prod = product.objects.get(id = id)
    prod.name = request.POST.get('name')
    prod.budgetedcost = request.POST.get('budgetedcost')
    prod.realcost = request.POST.get('realcost')
    shopid = request.POST.get('shop')
    _shop = shop.objects.get(id = shopid)
    prod.shop = _shop
    prod.notes = request.POST.get('notes')
    prod.save()
    return redirect('/shoppinglist/' + str(idlist) + '/')

def _productAlter(request, idlist, id):
    slist = shoppingList.objects.get(id = idlist)
    allprods = product.objects.all()
    prod = product.objects.get(id = id)

    if prod.bought == False:
        prod.bought = True
        prod.save()
    else:
        prod.bought = False
        prod.save()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def dontactdumb(request):
    logout(request)
    return redirect('/oauth/login/facebook/')

def base_layout(request):
	template='base.html'
	return render(request,template)