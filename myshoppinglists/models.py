from django.db import models
from django.contrib.auth.models import User

class shoppingList(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=150, null=False, blank=False)
    completed = models.BooleanField()
    def __str__(self):
        return self.name

class shop(models.Model):
    name = models.CharField(max_length=150, null=False, blank=False)
    name_sucursal = models.CharField(max_length=150, null=True, blank=True)
    address = models.CharField(max_length=150, null=False, blank=False)
    city = models.CharField(max_length=150, null=False, blank=False)
    region = models.CharField(max_length=150, null=False, blank=False)
    def __str__(self):
        return self.name

class product(models.Model):
    shopping_list = models.ForeignKey(shoppingList, on_delete=models.CASCADE)
    name = models.CharField(max_length=150, null=False, blank=False)
    budgetedcost = models.IntegerField(default=0)
    realcost = models.IntegerField(default=0)
    shop = models.ForeignKey(shop, on_delete=models.CASCADE)
    notes = models.CharField(max_length=150, null=True, blank=True)
    bought = models.BooleanField(default = False)
    def __str__(self):
        return self.name