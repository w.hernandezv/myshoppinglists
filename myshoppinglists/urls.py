"""myshoppinglists URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import views

from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views._index, name='index'),
    path('signin/', views._signin, name='signin'),
    path('signup/', views._signup, name='signup'),
    path('signup/create/', views._create, name='create'),
    path('login/', views._login, name='login'),
    path('logout/', views._logout, name='logout'),
    path('shoppinglist/create/', views._shoppingListCreate, name='shoppinglistcreate'),
    path('shoppinglist/update/<int:id>/', views._shoppingListUpdate, name='shoppinglistupdate'),
    path('shoppinglist/delete/<int:id>/', views._shoppingListDelete, name='shoppinglistdelete'),
    path('shoppinglist/<int:id>/', views._shoppingListRead, name='shoppinglistread'),
    path('shoppinglist/<int:idlist>/product/add/', views._productAdd, name='productadd'),
    path('shoppinglist/<int:idlist>/product/edit/<int:id>/', views._productEdit, name='productedit'),
    path('shoppinglist/<int:idlist>/product/delete/<int:id>/', views._productDelete, name='productdelete'),
    path('shoppinglist/<int:idlist>/product/edit/<int:id>/save/', views._productEditSave, name='producteditsave'),
    path('shoppinglist/<int:idlist>/product/alter/<int:id>/', views._productAlter, name='productAlter'),
    path('oauth/', include('social_django.urls', namespace='social')),  # <--
    path('',include('pwa.urls')),
    path('dontactdumb/', views.dontactdumb, name='dontactdumb'),
]
