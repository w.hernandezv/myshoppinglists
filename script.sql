BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `social_auth_usersocialauth` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`provider`	varchar ( 32 ) NOT NULL,
	`uid`	varchar ( 255 ) NOT NULL,
	`user_id`	integer NOT NULL,
	`extra_data`	text NOT NULL,
	FOREIGN KEY(`user_id`) REFERENCES `auth_user`(`id`) DEFERRABLE INITIALLY DEFERRED
);
INSERT INTO `social_auth_usersocialauth` VALUES (2,'facebook','10218833215129222',3,'{"auth_time": 1544075555, "id": "10218833215129222", "expires": 5179589, "granted_scopes": ["public_profile"], "denied_scopes": null, "access_token": "EAAKiBwQI2XEBAPf0wxZCZBVC53THgNork8SDOHvOBszZA8IxbxBYgT9A2Gkb9ZBhlXL2cZAX7apKGZBiajF4eZB7gY693ajrr7sBdzEDt5PnINovOT4vOi78BtjFtrH7Fmhl69OhbqMHHufEgh8ZCczEEAcDmkyFOA0tdbrP6F4x5QZDZD", "token_type": null}');
CREATE TABLE IF NOT EXISTS `social_auth_partial` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`token`	varchar ( 32 ) NOT NULL,
	`next_step`	smallint unsigned NOT NULL,
	`backend`	varchar ( 32 ) NOT NULL,
	`data`	text NOT NULL,
	`timestamp`	datetime NOT NULL
);
CREATE TABLE IF NOT EXISTS `social_auth_nonce` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`server_url`	varchar ( 255 ) NOT NULL,
	`timestamp`	integer NOT NULL,
	`salt`	varchar ( 65 ) NOT NULL
);
CREATE TABLE IF NOT EXISTS `social_auth_code` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`email`	varchar ( 254 ) NOT NULL,
	`code`	varchar ( 32 ) NOT NULL,
	`verified`	bool NOT NULL,
	`timestamp`	datetime NOT NULL
);
CREATE TABLE IF NOT EXISTS `social_auth_association` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`server_url`	varchar ( 255 ) NOT NULL,
	`handle`	varchar ( 255 ) NOT NULL,
	`secret`	varchar ( 255 ) NOT NULL,
	`issued`	integer NOT NULL,
	`lifetime`	integer NOT NULL,
	`assoc_type`	varchar ( 64 ) NOT NULL
);
CREATE TABLE IF NOT EXISTS `myshoppinglists_shoppinglist` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar ( 150 ) NOT NULL,
	`completed`	bool NOT NULL,
	`user_id`	integer NOT NULL,
	FOREIGN KEY(`user_id`) REFERENCES `auth_user`(`id`) DEFERRABLE INITIALLY DEFERRED
);
INSERT INTO `myshoppinglists_shoppinglist` VALUES (1,'Compras del mes',0,1);
CREATE TABLE IF NOT EXISTS `myshoppinglists_shop` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar ( 150 ) NOT NULL,
	`name_sucursal`	varchar ( 150 ),
	`address`	varchar ( 150 ) NOT NULL,
	`city`	varchar ( 150 ) NOT NULL,
	`region`	varchar ( 150 ) NOT NULL
);
INSERT INTO `myshoppinglists_shop` VALUES (1,'Pc Factory','Los Leones','Los Leones #136','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (2,'Falabella','Plaza Norte','Av. Americo Vespucio #1737','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (3,'Lider','Huechuraba','Av. Americo Vespucio #1737','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (4,'Tottus',NULL,'Pedro Fontova #5810','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (5,'Mayorista',NULL,' Catorce de La Fama #2841','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (6,'Microplay','Costanera','Av. Andrés Bello #2465','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (7,'Jumbo',NULL,'LLano subercaseaux #3519','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (8,'H&M',NULL,'Huerfanos #880','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (9,'Ripley','Huechuraba','Av. Americo Vespucio #1737','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (10,'La Polar',NULL,'Paseo puente #552','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (11,'Paris','Bandera','Bandera #201','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (12,'Forever 21','costanera',' Avenida Andres Bello #2425','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (13,'Weplay',NULL,'San Borja #66','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (14,'Arrow',NULL,'Andrés Bello #2465','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (15,'Santa Isabel','Independencia','Av. Independencia #3160','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (16,'Unimarc',NULL,'Gonzalo Bulnes #2407','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (17,'Hites',NULL,'Paseo Puente #696','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (18,'Johnson',NULL,'Agustinas #978','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (19,'Adidas','Plaza Norte','Av. Americo Vespucio #1737','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (20,'Mac Online',NULL,'Andrés Bello #2425','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (21,'Sparta',NULL,'San Francisco de Borja #122','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (22,'Bata',NULL,'Av. Americo Vespucio #1737','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (23,'Hush Puppies','Mall del centro','Paseo Puente #689','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (24,'Everlast',NULL,'Claudio Arrau #6910','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (25,'Pollini',NULL,'Estado #267','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (26,'Gacel',NULL,'Huerfanos #931','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (27,'Limonada',NULL,'Ahumada #319','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (28,'Rebook',NULL,'Av Presidente Kennedy #5413','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (29,'Flores',NULL,'moneda #1025','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (30,'Nike',NULL,'Av. Presidente Kennedy #9001','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (31,'Intime',NULL,'nataniel cox #1498','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (32,'DBS beauty store',NULL,'Av. Andrés Bello #2465','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (33,'Farmacia Cruz Verde',NULL,'Walker Martínez #1650','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (34,'Salcobrand',NULL,'Américo Vespucio #1737','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (35,'Farmacia Ahumada',NULL,'Av. Independencia #4142','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (36,'Samsung',NULL,'Agustinas #989','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (37,'Sony',NULL,'Av. Presidente Kennedy #8017','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (38,'Guante',NULL,'Av Presidente Kennedy #5413','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (39,'Zara',NULL,'Andrés Bello #2425','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (40,'Corona','Alameda Centro','Av Libertador Bernardo OHiggins #771','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (41,'Converse',NULL,'Andrés Bello #2425','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (42,'Vans',NULL,'Av. Nueva Providencia #2323','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (43,'rotter & krauss',NULL,'Av. Providencia #2056','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (44,'Maui and Sons',NULL,'Pedro Fontova #6251','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (45,'Casa Ideas',NULL,'Avda. Andrés Bello #2447','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (46,'Sybilla',NULL,'Ahumada #242','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (47,'New Balance',NULL,'Padre Hurtado Sur #875','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (48,'Head',NULL,'Andrés Bello #2447','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (49,'Saxoline',NULL,'Av. Providencia #2279','Santiago','Metropolitana');
INSERT INTO `myshoppinglists_shop` VALUES (50,'Morph','Costanera','Andres Bello #2447','Santiago','Metropolitana');
CREATE TABLE IF NOT EXISTS `myshoppinglists_product` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar ( 150 ) NOT NULL,
	`budgetedcost`	integer NOT NULL,
	`realcost`	integer NOT NULL,
	`notes`	varchar ( 150 ),
	`bought`	bool NOT NULL,
	`shop_id`	integer NOT NULL,
	`shopping_list_id`	integer NOT NULL,
	FOREIGN KEY(`shopping_list_id`) REFERENCES `myshoppinglists_shoppinglist`(`id`) DEFERRABLE INITIALLY DEFERRED,
	FOREIGN KEY(`shop_id`) REFERENCES `myshoppinglists_shop`(`id`) DEFERRABLE INITIALLY DEFERRED
);
INSERT INTO `myshoppinglists_product` VALUES (1,'Sandvich',1500,1100,'Notitas',0,2,1);
INSERT INTO `myshoppinglists_product` VALUES (5,'n_n',111,111,'aoeaoeaoe',0,9,1);
CREATE TABLE IF NOT EXISTS `django_session` (
	`session_key`	varchar ( 40 ) NOT NULL,
	`session_data`	text NOT NULL,
	`expire_date`	datetime NOT NULL,
	PRIMARY KEY(`session_key`)
);
INSERT INTO `django_session` VALUES ('nbgezxaymod1hjpr3eu1m15sq8jxsiq3','YWI4YWFmYmVlN2VkMmE2MTgxNDZmMzA5MDU5MWIxMzllNTI0M2ZjZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4NGUwNzU0NzdhNjc0YTA5OTIwN2FjMDBmZWUwMjI1NzZiMTM2YTNhIn0=','2018-12-20 00:48:16.013260');
INSERT INTO `django_session` VALUES ('iagky79woevs9ibrukdfdk4vtghcvxmk','ZjdmZjE1ZDUzMmIyOGE3ZmZlNTA0Yzg2MmY3ODhlZmI0Y2JjNjc4MTp7ImZhY2Vib29rX3N0YXRlIjoiZHdwamFHTThGcEltWm5hbVhRUEI1RVhiOFptbzBrN28ifQ==','2018-12-20 04:29:57.671165');
INSERT INTO `django_session` VALUES ('fn9jmqpew6y75w3oc6l06ok3bavqe3ym','YWI4YWFmYmVlN2VkMmE2MTgxNDZmMzA5MDU5MWIxMzllNTI0M2ZjZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4NGUwNzU0NzdhNjc0YTA5OTIwN2FjMDBmZWUwMjI1NzZiMTM2YTNhIn0=','2018-12-20 07:22:33.004387');
CREATE TABLE IF NOT EXISTS `django_migrations` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`app`	varchar ( 255 ) NOT NULL,
	`name`	varchar ( 255 ) NOT NULL,
	`applied`	datetime NOT NULL
);
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2018-12-06 00:46:57.903189');
INSERT INTO `django_migrations` VALUES (2,'auth','0001_initial','2018-12-06 00:46:57.936312');
INSERT INTO `django_migrations` VALUES (3,'admin','0001_initial','2018-12-06 00:46:57.966056');
INSERT INTO `django_migrations` VALUES (4,'admin','0002_logentry_remove_auto_add','2018-12-06 00:46:57.999785');
INSERT INTO `django_migrations` VALUES (5,'admin','0003_logentry_add_action_flag_choices','2018-12-06 00:46:58.030964');
INSERT INTO `django_migrations` VALUES (6,'contenttypes','0002_remove_content_type_name','2018-12-06 00:46:58.074262');
INSERT INTO `django_migrations` VALUES (7,'auth','0002_alter_permission_name_max_length','2018-12-06 00:46:58.100265');
INSERT INTO `django_migrations` VALUES (8,'auth','0003_alter_user_email_max_length','2018-12-06 00:46:58.133386');
INSERT INTO `django_migrations` VALUES (9,'auth','0004_alter_user_username_opts','2018-12-06 00:46:58.165562');
INSERT INTO `django_migrations` VALUES (10,'auth','0005_alter_user_last_login_null','2018-12-06 00:46:58.195598');
INSERT INTO `django_migrations` VALUES (11,'auth','0006_require_contenttypes_0002','2018-12-06 00:46:58.207629');
INSERT INTO `django_migrations` VALUES (12,'auth','0007_alter_validators_add_error_messages','2018-12-06 00:46:58.235618');
INSERT INTO `django_migrations` VALUES (13,'auth','0008_alter_user_username_max_length','2018-12-06 00:46:58.267546');
INSERT INTO `django_migrations` VALUES (14,'auth','0009_alter_user_last_name_max_length','2018-12-06 00:46:58.300098');
INSERT INTO `django_migrations` VALUES (15,'sessions','0001_initial','2018-12-06 00:46:58.324851');
INSERT INTO `django_migrations` VALUES (16,'myshoppinglists','0001_initial','2018-12-06 00:47:26.366752');
INSERT INTO `django_migrations` VALUES (17,'default','0001_initial','2018-12-06 04:30:33.207637');
INSERT INTO `django_migrations` VALUES (18,'social_auth','0001_initial','2018-12-06 04:30:33.218667');
INSERT INTO `django_migrations` VALUES (19,'default','0002_add_related_name','2018-12-06 04:30:33.245739');
INSERT INTO `django_migrations` VALUES (20,'social_auth','0002_add_related_name','2018-12-06 04:30:33.253760');
INSERT INTO `django_migrations` VALUES (21,'default','0003_alter_email_max_length','2018-12-06 04:30:33.275819');
INSERT INTO `django_migrations` VALUES (22,'social_auth','0003_alter_email_max_length','2018-12-06 04:30:33.283840');
INSERT INTO `django_migrations` VALUES (23,'default','0004_auto_20160423_0400','2018-12-06 04:30:33.311066');
INSERT INTO `django_migrations` VALUES (24,'social_auth','0004_auto_20160423_0400','2018-12-06 04:30:33.321094');
INSERT INTO `django_migrations` VALUES (25,'social_auth','0005_auto_20160727_2333','2018-12-06 04:30:33.340720');
INSERT INTO `django_migrations` VALUES (26,'social_django','0006_partial','2018-12-06 04:30:33.363181');
INSERT INTO `django_migrations` VALUES (27,'social_django','0007_code_timestamp','2018-12-06 04:30:33.389384');
INSERT INTO `django_migrations` VALUES (28,'social_django','0008_partial_timestamp','2018-12-06 04:30:33.413476');
INSERT INTO `django_migrations` VALUES (29,'social_django','0001_initial','2018-12-06 04:30:33.425481');
INSERT INTO `django_migrations` VALUES (30,'social_django','0003_alter_email_max_length','2018-12-06 04:30:33.435538');
INSERT INTO `django_migrations` VALUES (31,'social_django','0005_auto_20160727_2333','2018-12-06 04:30:33.444589');
INSERT INTO `django_migrations` VALUES (32,'social_django','0002_add_related_name','2018-12-06 04:30:33.453586');
INSERT INTO `django_migrations` VALUES (33,'social_django','0004_auto_20160423_0400','2018-12-06 04:30:33.462611');
CREATE TABLE IF NOT EXISTS `django_content_type` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`app_label`	varchar ( 100 ) NOT NULL,
	`model`	varchar ( 100 ) NOT NULL
);
INSERT INTO `django_content_type` VALUES (1,'admin','logentry');
INSERT INTO `django_content_type` VALUES (2,'auth','permission');
INSERT INTO `django_content_type` VALUES (3,'auth','group');
INSERT INTO `django_content_type` VALUES (4,'auth','user');
INSERT INTO `django_content_type` VALUES (5,'contenttypes','contenttype');
INSERT INTO `django_content_type` VALUES (6,'sessions','session');
INSERT INTO `django_content_type` VALUES (7,'myshoppinglists','shoppinglist');
INSERT INTO `django_content_type` VALUES (8,'myshoppinglists','shop');
INSERT INTO `django_content_type` VALUES (9,'myshoppinglists','product');
INSERT INTO `django_content_type` VALUES (10,'social_django','association');
INSERT INTO `django_content_type` VALUES (11,'social_django','code');
INSERT INTO `django_content_type` VALUES (12,'social_django','nonce');
INSERT INTO `django_content_type` VALUES (13,'social_django','usersocialauth');
INSERT INTO `django_content_type` VALUES (14,'social_django','partial');
CREATE TABLE IF NOT EXISTS `django_admin_log` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`action_time`	datetime NOT NULL,
	`object_id`	text,
	`object_repr`	varchar ( 200 ) NOT NULL,
	`change_message`	text NOT NULL,
	`content_type_id`	integer,
	`user_id`	integer NOT NULL,
	`action_flag`	smallint unsigned NOT NULL,
	FOREIGN KEY(`user_id`) REFERENCES `auth_user`(`id`) DEFERRABLE INITIALLY DEFERRED,
	FOREIGN KEY(`content_type_id`) REFERENCES `django_content_type`(`id`) DEFERRABLE INITIALLY DEFERRED
);
INSERT INTO `django_admin_log` VALUES (1,'2018-12-06 04:36:29.326499','1','Alexander','',13,1,3);
INSERT INTO `django_admin_log` VALUES (2,'2018-12-06 04:36:39.379259','2','Alexander','',4,1,3);
INSERT INTO `django_admin_log` VALUES (3,'2018-12-06 06:33:11.300563','1','Compras del mes','[{"changed": {"fields": ["name"]}}]',7,1,2);
CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`user_id`	integer NOT NULL,
	`permission_id`	integer NOT NULL,
	FOREIGN KEY(`user_id`) REFERENCES `auth_user`(`id`) DEFERRABLE INITIALLY DEFERRED,
	FOREIGN KEY(`permission_id`) REFERENCES `auth_permission`(`id`) DEFERRABLE INITIALLY DEFERRED
);
CREATE TABLE IF NOT EXISTS `auth_user_groups` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`user_id`	integer NOT NULL,
	`group_id`	integer NOT NULL,
	FOREIGN KEY(`group_id`) REFERENCES `auth_group`(`id`) DEFERRABLE INITIALLY DEFERRED,
	FOREIGN KEY(`user_id`) REFERENCES `auth_user`(`id`) DEFERRABLE INITIALLY DEFERRED
);
CREATE TABLE IF NOT EXISTS `auth_user` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`password`	varchar ( 128 ) NOT NULL,
	`last_login`	datetime,
	`is_superuser`	bool NOT NULL,
	`username`	varchar ( 150 ) NOT NULL UNIQUE,
	`first_name`	varchar ( 30 ) NOT NULL,
	`email`	varchar ( 254 ) NOT NULL,
	`is_staff`	bool NOT NULL,
	`is_active`	bool NOT NULL,
	`date_joined`	datetime NOT NULL,
	`last_name`	varchar ( 150 ) NOT NULL
);
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$120000$XfvWt3mVl6qt$xvhRRaJ0607otBWa03TOlolffX1lVepR3B9iQ4RBg1Y=','2018-12-06 07:22:32.997369',1,'admin','','',1,1,'2018-12-06 00:47:48.329226','');
INSERT INTO `auth_user` VALUES (3,'!5rv1cWtlG8PbK2qlneBB0rYVSkbwNgTSOdh4TFEP','2018-12-06 05:52:35.563926',0,'Alexander','Alexander','',0,1,'2018-12-06 04:39:04.459039','');
CREATE TABLE IF NOT EXISTS `auth_permission` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`content_type_id`	integer NOT NULL,
	`codename`	varchar ( 100 ) NOT NULL,
	`name`	varchar ( 255 ) NOT NULL,
	FOREIGN KEY(`content_type_id`) REFERENCES `django_content_type`(`id`) DEFERRABLE INITIALLY DEFERRED
);
INSERT INTO `auth_permission` VALUES (1,1,'add_logentry','Can add log entry');
INSERT INTO `auth_permission` VALUES (2,1,'change_logentry','Can change log entry');
INSERT INTO `auth_permission` VALUES (3,1,'delete_logentry','Can delete log entry');
INSERT INTO `auth_permission` VALUES (4,1,'view_logentry','Can view log entry');
INSERT INTO `auth_permission` VALUES (5,2,'add_permission','Can add permission');
INSERT INTO `auth_permission` VALUES (6,2,'change_permission','Can change permission');
INSERT INTO `auth_permission` VALUES (7,2,'delete_permission','Can delete permission');
INSERT INTO `auth_permission` VALUES (8,2,'view_permission','Can view permission');
INSERT INTO `auth_permission` VALUES (9,3,'add_group','Can add group');
INSERT INTO `auth_permission` VALUES (10,3,'change_group','Can change group');
INSERT INTO `auth_permission` VALUES (11,3,'delete_group','Can delete group');
INSERT INTO `auth_permission` VALUES (12,3,'view_group','Can view group');
INSERT INTO `auth_permission` VALUES (13,4,'add_user','Can add user');
INSERT INTO `auth_permission` VALUES (14,4,'change_user','Can change user');
INSERT INTO `auth_permission` VALUES (15,4,'delete_user','Can delete user');
INSERT INTO `auth_permission` VALUES (16,4,'view_user','Can view user');
INSERT INTO `auth_permission` VALUES (17,5,'add_contenttype','Can add content type');
INSERT INTO `auth_permission` VALUES (18,5,'change_contenttype','Can change content type');
INSERT INTO `auth_permission` VALUES (19,5,'delete_contenttype','Can delete content type');
INSERT INTO `auth_permission` VALUES (20,5,'view_contenttype','Can view content type');
INSERT INTO `auth_permission` VALUES (21,6,'add_session','Can add session');
INSERT INTO `auth_permission` VALUES (22,6,'change_session','Can change session');
INSERT INTO `auth_permission` VALUES (23,6,'delete_session','Can delete session');
INSERT INTO `auth_permission` VALUES (24,6,'view_session','Can view session');
INSERT INTO `auth_permission` VALUES (25,7,'add_shoppinglist','Can add shopping list');
INSERT INTO `auth_permission` VALUES (26,7,'change_shoppinglist','Can change shopping list');
INSERT INTO `auth_permission` VALUES (27,7,'delete_shoppinglist','Can delete shopping list');
INSERT INTO `auth_permission` VALUES (28,7,'view_shoppinglist','Can view shopping list');
INSERT INTO `auth_permission` VALUES (29,8,'add_shop','Can add shop');
INSERT INTO `auth_permission` VALUES (30,8,'change_shop','Can change shop');
INSERT INTO `auth_permission` VALUES (31,8,'delete_shop','Can delete shop');
INSERT INTO `auth_permission` VALUES (32,8,'view_shop','Can view shop');
INSERT INTO `auth_permission` VALUES (33,9,'add_product','Can add product');
INSERT INTO `auth_permission` VALUES (34,9,'change_product','Can change product');
INSERT INTO `auth_permission` VALUES (35,9,'delete_product','Can delete product');
INSERT INTO `auth_permission` VALUES (36,9,'view_product','Can view product');
INSERT INTO `auth_permission` VALUES (37,10,'add_association','Can add association');
INSERT INTO `auth_permission` VALUES (38,10,'change_association','Can change association');
INSERT INTO `auth_permission` VALUES (39,10,'delete_association','Can delete association');
INSERT INTO `auth_permission` VALUES (40,10,'view_association','Can view association');
INSERT INTO `auth_permission` VALUES (41,11,'add_code','Can add code');
INSERT INTO `auth_permission` VALUES (42,11,'change_code','Can change code');
INSERT INTO `auth_permission` VALUES (43,11,'delete_code','Can delete code');
INSERT INTO `auth_permission` VALUES (44,11,'view_code','Can view code');
INSERT INTO `auth_permission` VALUES (45,12,'add_nonce','Can add nonce');
INSERT INTO `auth_permission` VALUES (46,12,'change_nonce','Can change nonce');
INSERT INTO `auth_permission` VALUES (47,12,'delete_nonce','Can delete nonce');
INSERT INTO `auth_permission` VALUES (48,12,'view_nonce','Can view nonce');
INSERT INTO `auth_permission` VALUES (49,13,'add_usersocialauth','Can add user social auth');
INSERT INTO `auth_permission` VALUES (50,13,'change_usersocialauth','Can change user social auth');
INSERT INTO `auth_permission` VALUES (51,13,'delete_usersocialauth','Can delete user social auth');
INSERT INTO `auth_permission` VALUES (52,13,'view_usersocialauth','Can view user social auth');
INSERT INTO `auth_permission` VALUES (53,14,'add_partial','Can add partial');
INSERT INTO `auth_permission` VALUES (54,14,'change_partial','Can change partial');
INSERT INTO `auth_permission` VALUES (55,14,'delete_partial','Can delete partial');
INSERT INTO `auth_permission` VALUES (56,14,'view_partial','Can view partial');
CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`group_id`	integer NOT NULL,
	`permission_id`	integer NOT NULL,
	FOREIGN KEY(`group_id`) REFERENCES `auth_group`(`id`) DEFERRABLE INITIALLY DEFERRED,
	FOREIGN KEY(`permission_id`) REFERENCES `auth_permission`(`id`) DEFERRABLE INITIALLY DEFERRED
);
CREATE TABLE IF NOT EXISTS `auth_group` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar ( 80 ) NOT NULL UNIQUE
);
CREATE INDEX IF NOT EXISTS `social_auth_usersocialauth_user_id_17d28448` ON `social_auth_usersocialauth` (
	`user_id`
);
CREATE UNIQUE INDEX IF NOT EXISTS `social_auth_usersocialauth_provider_uid_e6b5e668_uniq` ON `social_auth_usersocialauth` (
	`provider`,
	`uid`
);
CREATE INDEX IF NOT EXISTS `social_auth_partial_token_3017fea3` ON `social_auth_partial` (
	`token`
);
CREATE INDEX IF NOT EXISTS `social_auth_partial_timestamp_50f2119f` ON `social_auth_partial` (
	`timestamp`
);
CREATE UNIQUE INDEX IF NOT EXISTS `social_auth_nonce_server_url_timestamp_salt_f6284463_uniq` ON `social_auth_nonce` (
	`server_url`,
	`timestamp`,
	`salt`
);
CREATE INDEX IF NOT EXISTS `social_auth_code_timestamp_176b341f` ON `social_auth_code` (
	`timestamp`
);
CREATE UNIQUE INDEX IF NOT EXISTS `social_auth_code_email_code_801b2d02_uniq` ON `social_auth_code` (
	`email`,
	`code`
);
CREATE INDEX IF NOT EXISTS `social_auth_code_code_a2393167` ON `social_auth_code` (
	`code`
);
CREATE UNIQUE INDEX IF NOT EXISTS `social_auth_association_server_url_handle_078befa2_uniq` ON `social_auth_association` (
	`server_url`,
	`handle`
);
CREATE INDEX IF NOT EXISTS `myshoppinglists_shoppinglist_user_id_c28856ba` ON `myshoppinglists_shoppinglist` (
	`user_id`
);
CREATE INDEX IF NOT EXISTS `myshoppinglists_product_shopping_list_id_6e2fe2eb` ON `myshoppinglists_product` (
	`shopping_list_id`
);
CREATE INDEX IF NOT EXISTS `myshoppinglists_product_shop_id_c6369c17` ON `myshoppinglists_product` (
	`shop_id`
);
CREATE INDEX IF NOT EXISTS `django_session_expire_date_a5c62663` ON `django_session` (
	`expire_date`
);
CREATE UNIQUE INDEX IF NOT EXISTS `django_content_type_app_label_model_76bd3d3b_uniq` ON `django_content_type` (
	`app_label`,
	`model`
);
CREATE INDEX IF NOT EXISTS `django_admin_log_user_id_c564eba6` ON `django_admin_log` (
	`user_id`
);
CREATE INDEX IF NOT EXISTS `django_admin_log_content_type_id_c4bce8eb` ON `django_admin_log` (
	`content_type_id`
);
CREATE UNIQUE INDEX IF NOT EXISTS `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` ON `auth_user_user_permissions` (
	`user_id`,
	`permission_id`
);
CREATE INDEX IF NOT EXISTS `auth_user_user_permissions_user_id_a95ead1b` ON `auth_user_user_permissions` (
	`user_id`
);
CREATE INDEX IF NOT EXISTS `auth_user_user_permissions_permission_id_1fbb5f2c` ON `auth_user_user_permissions` (
	`permission_id`
);
CREATE UNIQUE INDEX IF NOT EXISTS `auth_user_groups_user_id_group_id_94350c0c_uniq` ON `auth_user_groups` (
	`user_id`,
	`group_id`
);
CREATE INDEX IF NOT EXISTS `auth_user_groups_user_id_6a12ed8b` ON `auth_user_groups` (
	`user_id`
);
CREATE INDEX IF NOT EXISTS `auth_user_groups_group_id_97559544` ON `auth_user_groups` (
	`group_id`
);
CREATE UNIQUE INDEX IF NOT EXISTS `auth_permission_content_type_id_codename_01ab375a_uniq` ON `auth_permission` (
	`content_type_id`,
	`codename`
);
CREATE INDEX IF NOT EXISTS `auth_permission_content_type_id_2f476e4b` ON `auth_permission` (
	`content_type_id`
);
CREATE INDEX IF NOT EXISTS `auth_group_permissions_permission_id_84c5c92e` ON `auth_group_permissions` (
	`permission_id`
);
CREATE UNIQUE INDEX IF NOT EXISTS `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` ON `auth_group_permissions` (
	`group_id`,
	`permission_id`
);
CREATE INDEX IF NOT EXISTS `auth_group_permissions_group_id_b120cbf9` ON `auth_group_permissions` (
	`group_id`
);
COMMIT;
